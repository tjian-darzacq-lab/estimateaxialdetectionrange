%   ModelFit_AxialDetectionRange.m
%   Anders Sejr Hansen & Maxime Woringer, Oct 2017
clear; clc; close all; clearvars -global

% Define global variables
global minTrajLength

%   DESCRIPTION:
%   This script is useful for determing your axial detection range. 
%   Collect a z-stack of single-molecules in a fixed cell under the same
%   imaging conditons (i.e. signal-to-noise) as your do your
%   single-particle tracking experiments. Then track molecules between 
%   frames and generate trajectories. Then set a minimum length and
%   throw out trajectories below this minimal length. Then calculate a
%   survival probability as a function of the frame number. This script
%   will then fit your empirical survival probability curve and estimate
%   the axial detection range of your system. In this example, we will use
%   our own raw data, a variable termed SurvivalProb. In this example, the
%   z-stack step size was 20 nm and the minimum trajectory length was
%   either 10 or 15 frames and the experiment was performed in either human
%   U2OS cells (DataSet 3,4) or mouse ES cells (DataSet 1,2). 
%
%   This script requires the function "DeltaZ_GaussModel.m" to be in the
%   same folder in order to run. 
%
%   If you use this script to perform your own experiments, please change
%   the parameters accordingly. 

%   CAVEATs:
%       We would like to emphasize that although estimating the axial detection
%   range intuitively seems relatively straightforward, it is actually a
%   little complicated. Here we took a z-stack spanning the whole nucleus
%   to maximize the amount for data and here are some of the limitations
%   and approximations associated with the experiment and analysis:
%       1. Consider a molecules at the very edge of the detection range (say
%   10% detection probability). When during the z-stack, the objective
%   moves closer to the actual focal plane, the detection probability will
%   increase to say 20% and then to ~100% eventually. However, although the
%   probability increases, it is quite likely that the molecule will be
%   missed and thus this will contribute to several short trajectories.
%   However, this should largely be filtered out during the dataprocessing
%   since we set a minimum trajectory length of 10-15 frames.
%       2. This caveat highligts how the model is a simplification. We model a
%   single Gaussian CDF edge, but really both edges are Gaussian CDFs and a
%   more complete model would account for this. 
%       3. We observe some level of false positives in the tracking: these
%   would cause the photo-bleaching estimate to fail so we filter them out
%   by setting a minimum trajectory threshold. 
%       4. The detection probability is very localization algorithm dependent
%   and modeling it as a Gaussian CDF seems reasonable, but it is an
%   approximation. 
%       5. Tracking errors: although we performed the z-stacks at low density
%   and was very conservative in how far the molecules would be allowed to
%   move between frames, low-frequency tracking errors may contribute
%   artificially long trajectories, which can bias the estimate.
%       6. Nevertheless, we note (see Supplementary Figure "Sensitivity of
%   Spot-On to the axial detection range estimate." that Spot-On is not
%   very sensitive to the axial detection range. Even if it is misestimated
%   by 100-200 nm, it does not bias the parameter estimation of D_FREE or
%   F_BOUND very much. 

zStepSize = 20; %in nanometers
FitIterations = 3;
DataSet = 4;

if DataSet == 1; % considering only trajectories of at least 10 frames
    SurvivalProb = [1,1,1,1,1,1,1,1,1,1,0.962630792227205,0.933233682112606,0.905331340308919,0.873941205779771,0.852017937219731,0.833084205281515,0.816641753861485,0.800697558545092,0.778276033881415,0.762331838565022,0.744394618834081,0.725959142999502,0.704035874439462,0.684603886397608,0.661684105630294,0.640259093173891,0.614848031888391,0.594917787742900,0.577977080219233,0.554559043348281,0.532635774788241,0.506726457399103,0.488290981564524,0.468858993522671,0.443946188340807,0.422521175884405,0.400099651220727,0.375685102142501,0.342800199302441,0.315894369706029,0.277030393622322,0.242152466367713,0.201295465869457,0.170403587443946,0.141006477329347,0.122072745391131,0.102142501245640,0.0886895864474339,0.0777279521674139,0.0682610861983057,0.0622820129546585,0.0523168908819132,0.0478325859491777,0.0443447932237169,0.0403587443946187,0.0368709516691579,0.0348779272546088,0.0323866467364224,0.0288988540109616,0.0279023418036870,0.0244145490782262,0.0199302441454907,0.0179372197309416,0.0164424514200299,0.0154459392127553,0.0139511709018435,0.0124564025909317,0.0114598903836571,0.0114598903836571,0.0109616342800198,0.00946686596910806,0.00896860986547077,0.00747384155455899,0.00747384155455899,0.00747384155455899,0.00697558545092170,0.00697558545092170,0.00647732934728440,0.00597907324364710,0.00448430493273533,0.00448430493273533,0.00448430493273533,0.00398604882909803,0.00348779272546074,0.00348779272546074,0.00348779272546074,0.00348779272546074,0.00249128051818615,0.00199302441454885,0.00199302441454885,0.00199302441454885,0.00199302441454885,0.000996512207274258,0.000996512207274258,0.000996512207274258,0.000996512207274258,0.000996512207274258,0.000498256103636963,0.000498256103636963,0.000498256103636963,-2.22044604925031e-16];
    minTrajLength = 10;
    SampleName = 'mESC C59 Halo-mCTCF; PFA-fixed; JF646; 20 nm z-stack; minTrajLength 10';
elseif DataSet == 2 % considering only trajectories of at least 15 frames
    SurvivalProb = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0.977777777777778,0.958479532163743,0.939766081871345,0.913450292397661,0.894736842105263,0.873684210526316,0.852046783625731,0.826315789473684,0.803508771929825,0.776608187134503,0.751461988304094,0.721637426900585,0.698245614035088,0.678362573099415,0.650877192982456,0.625146198830409,0.594736842105263,0.573099415204678,0.550292397660819,0.521052631578947,0.495906432748538,0.469590643274854,0.440935672514620,0.402339181286550,0.370760233918129,0.325146198830409,0.284210526315789,0.236257309941521,0.200000000000000,0.165497076023392,0.143274853801170,0.119883040935673,0.104093567251462,0.0912280701754387,0.0801169590643276,0.0730994152046784,0.0614035087719299,0.0561403508771930,0.0520467836257310,0.0473684210526316,0.0432748538011696,0.0409356725146198,0.0380116959064327,0.0339181286549707,0.0327485380116959,0.0286549707602338,0.0233918128654970,0.0210526315789472,0.0192982456140349,0.0181286549707601,0.0163742690058478,0.0146198830409355,0.0134502923976607,0.0134502923976607,0.0128654970760232,0.0111111111111110,0.0105263157894735,0.00877192982456121,0.00877192982456121,0.00877192982456121,0.00818713450292374,0.00818713450292374,0.00760233918128628,0.00701754385964881,0.00526315789473653,0.00526315789473653,0.00526315789473653,0.00467836257309906,0.00409356725146159,0.00409356725146159,0.00409356725146159,0.00409356725146159,0.00292397660818677,0.00233918128654931,0.00233918128654931,0.00233918128654931,0.00233918128654931,0.00116959064327449,0.00116959064327449,0.00116959064327449,0.00116959064327449,0.00116959064327449,0.000584795321637022,0.000584795321637022,0.000584795321637022,-4.44089209850063e-16];
    minTrajLength = 15;
    SampleName = 'mESC C59 Halo-mCTCF; PFA-fixed; JF646; 20 nm z-stack; minTrajLength 15';
elseif DataSet == 3 % considering only trajectories of at least 10 frames U2OS
    SurvivalProb = [1,1,1,1,1,1,1,1,1,1,0.960043588812205,0.924082818743189,0.895386850708318,0.865601162368326,0.837268434435162,0.802760624772975,0.770069015619324,0.739193606974210,0.711950599346168,0.677442789683981,0.642571739920087,0.610243370868144,0.580094442426444,0.541227751543771,0.508172902288413,0.472212132219397,0.434435161641845,0.399564111877951,0.364693062114057,0.329822012350163,0.296403922993098,0.267707954958227,0.241191427533599,0.213585179803850,0.189248092989466,0.159099164547766,0.137304758445332,0.120232473665092,0.103886669088267,0.0918997457319285,0.0824555030875406,0.0733745005448598,0.0639302579004719,0.0552124954594985,0.0523065746458407,0.0450417726116961,0.0421358517980383,0.0385034507809660,0.0370504903741370,0.0352342898656008,0.0319651289502357,0.0286959680348706,0.0261532873229200,0.0247003269160910,0.0228841264075549,0.0203414456956043,0.0188884852887753,0.0170722847802391,0.0145296040682885,0.0119869233563379,0.0108972030512162,0.00944424264438726,0.00799128223755830,0.00653832183072933,0.00544860152560767,0.00544860152560767,0.00435888122048600,0.00399564111877881,0.00399564111877881,0.00326916091536433,0.00290592081365715,0.00254268071194996,0.00217944061024278,0.00217944061024278,0.00217944061024278,0.00145296040682830,0.000726480203413815,0.000726480203413815,0.000726480203413815,0.000726480203413815,-6.66133814775094e-16];
    minTrajLength = 10;
    SampleName = 'U2OS C32 Halo-hCTCF; PFA-fixed; JF646; 20 nm z-stack; minTrajLength 10';
elseif DataSet == 4 % considering only trajectories of at least 15 frames U2OS
    SurvivalProb = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0.958785249457701,0.919739696312364,0.882863340563991,0.850325379609545,0.809110629067245,0.767462039045553,0.728850325379610,0.692841648590022,0.646420824295011,0.606941431670282,0.563991323210412,0.518872017353579,0.477223427331887,0.435574837310195,0.393926247288503,0.354013015184382,0.319739696312365,0.288069414316703,0.255097613882863,0.226030368763558,0.190021691973970,0.163991323210412,0.143600867678959,0.124078091106291,0.109761388286334,0.0984815618221261,0.0876355748373104,0.0763557483731022,0.0659436008676793,0.0624728850325382,0.0537960954446858,0.0503253796095448,0.0459869848156185,0.0442516268980480,0.0420824295010849,0.0381778741865513,0.0342733188720177,0.0312364425162693,0.0295010845986988,0.0273318872017357,0.0242950108459874,0.0225596529284169,0.0203904555314538,0.0173535791757055,0.0143167028199571,0.0130151843817793,0.0112798264642088,0.00954446854663826,0.00780911062906775,0.00650759219088992,0.00650759219088992,0.00520607375271209,0.00477223427331952,0.00477223427331952,0.00390455531453426,0.00347071583514169,0.00303687635574912,0.00260303687635655,0.00260303687635655,0.00260303687635655,0.00173535791757129,0.000867678958786033,0.000867678958786033,0.000867678958786033,0.000867678958786033,7.77156117237610e-16];
    minTrajLength = 15;
    SampleName = 'U2OS C32 Halo-hCTCF; PFA-fixed; JF646; 20 nm z-stack; minTrajLength 15';
end


% model-fitting stuff
frame_vector = 1:length(SurvivalProb);
list_of_model_parameters = {'k_photobleach', 'z_frames', 'sigma_z'};
k_photobleach_range = [0, 0.05];
z_frames_range = [0 100];
sigma_z_range = [0 50];

LB = [k_photobleach_range(1), z_frames_range(1), sigma_z_range(1)];
UB = [k_photobleach_range(2), z_frames_range(2), sigma_z_range(2)];
diff_bounds = UB - LB; %difference: used for initial parameters guess
best_ssq2 = 5e10; %initial error
% Options for the non-linear least squares parameter optimisation
options = optimset('MaxIter',1000,'MaxFunEvals', 5000, 'TolFun',1e-8,'TolX',1e-8,'Display','on');

for iter=1:FitIterations
    %Guess a random set of parameters
    parameter_guess =rand(1,length(LB)).*diff_bounds+LB; 
    
    % Perform actual Least-Squares fitting
    [values, ssq2, residuals] = lsqcurvefit('DeltaZ_GaussModel', parameter_guess, frame_vector, SurvivalProb, LB, UB, options);
    
    % See if the current fit is an improvement:
    if ssq2 < best_ssq2
        best_vals = values; 
        best_ssq2 = ssq2;
        %OUTPUT THE NEW BEST VALUES TO THE SCREEN
        disp('==================================================');
        disp(['Improved fit on iteration ', num2str(iter)]);
        disp(['Improved error is ', num2str(ssq2)]);
        for k = 1:length(best_vals)
            disp([char(list_of_model_parameters{k}), ' = ', num2str(best_vals(k))]);
        end
        disp('==================================================');
    else
        disp(['Iteration ', num2str(iter), ' did not yield an improved fit']);
    end
end

% Get a final output of the fit:
fit_SurvivalProb  = DeltaZ_GaussModel( best_vals, SurvivalProb );


figure1 = figure('position',[200 200 300 275]); %[x y width height] 
hold on;
for i=2:length(frame_vector)
    x1 = frame_vector(1,i-1)*zStepSize; x2 = frame_vector(1,i)*zStepSize;
    y = SurvivalProb(1,i-1);
    patch([x1 x1 x2 x2], [0 y y 0], [237/255, 28/255, 36/255],'LineStyle','none');
end
plot(frame_vector*zStepSize, fit_SurvivalProb, 'k-', 'LineWidth', 2);
axis([zStepSize 1600 0 1.02]);
title({SampleName; ['Inferred mean axial detection range: ', num2str(best_vals(2)*zStepSize), ' nm']}, 'FontSize',9, 'FontName', 'Helvetica');
ylabel('survival probability', 'FontSize',8, 'FontName', 'Helvetica');
xlabel('z-stack range (nm)', 'FontSize',8, 'FontName', 'Helvetica');
hold off;
set(figure1,'Units','Inches');
pos = get(figure1,'Position');
set(figure1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
print(figure1,[SampleName, 'AxialDetectionRange.pdf'],'-dpdf','-r0');





