function [ fit_SurvivalProb ] = DeltaZ_GaussModel( parameter_guess, SurvivalProb )
%DELTAZ_GAUSSMODEL 
global minTrajLength
%   This is a model for the axial detection range, that assummes Poissonian
%   photobleaching and that the axial detection range has Gaussian edges. 

% parameter values:
k_photobleach = parameter_guess(1);
z_frames = parameter_guess(2);
sigma_z = parameter_guess(3);

fit_SurvivalProb = zeros(1,length(SurvivalProb));
frames = 1:length(fit_SurvivalProb);

% as long as it is less than the minTrajLength:
fit_SurvivalProb(1:minTrajLength) = ones(1,minTrajLength);
% if it is more than minTrajLength:
fit_SurvivalProb(minTrajLength+1:end) = (exp(-k_photobleach.*(frames(minTrajLength+1:end)-minTrajLength))) ...
                                        .* 0.5.*( 1 - erf( ((frames(minTrajLength+1:end)-minTrajLength) - (z_frames-minTrajLength))./(sigma_z*sqrt(2))));

end

