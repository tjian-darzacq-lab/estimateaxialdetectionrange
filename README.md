Estimate Axial Detection Range
--------------------------

# Overview
This repository contains a simple Matlab script to estimate the axial
detection range. The main assumptions of the model are that:

- photobleaching is a Poisson Process
- the detection range can be modeled as a Gaussian CDF

The main script, "ModelFit_AxialDetectionRange.m" contains the
experimental survival probability that we estimated (see DataSet
1,2,3,4) and reads in one of these and then fits the model and plots
the resulting output. 
To use it on your own data, collect a z-stack of single-molecules in a
fixed cell under the same imaging conditons (i.e. signal-to-noise) as
your do your single-particle tracking experiments. Then track
molecules between frames and generate trajectories. Then set a minimum
length and throw out trajectories below this minimal length. Then
calculate a survival probability as a function of the frame
number. This script will then fit your empirical survival probability curve and estimate the axial detection range of your system.

CAVEATs:
We would like to emphasize that although estimating the axial detection
range intuitively seems relatively straightforward, it is actually a
little complicated. Here we took a z-stack spanning the whole nucleus
to maximize the amount for data and here are some of the limitations
and approximations associated with the experiment and analysis:

1. Consider a molecules at the very edge of the detection range
	   (say 10% detection probability). When during the z-stack, the
	   objective moves closer to the actual focal plane, the detection probability will increase to say 20% and then to ~100% eventually. However, although the probability increases, it is quite likely that the molecule will be  missed and thus this will contribute to several short trajectories. However, this should largely be filtered out during the dataprocessing  since we set a minimum trajectory length of 10-15 frames.

2. This caveat highligts how the model is a simplification. We model a single Gaussian CDF edge, but really both edges are Gaussian CDFs and a more complete model would account for this. 

3. We observe some level of false positives in the tracking: these would cause the photo-bleaching estimate to fail so we filter them out  by setting a minimum trajectory threshold. 

4. The detection probability is very localization algorithm dependent and modeling it as a Gaussian CDF seems reasonable, but it is an approximation.

5. Tracking errors: although we performed the z-stacks at low
	   density and was very conservative in how far the molecules would be allowed to move between frames, low-frequency tracking errors may contribute artificially long trajectories, which can bias the estimate.

6. Nevertheless, we note (see Supplementary Figure "Sensitivity
	   of Spot-On to the axial detection range estimate." that Spot-On
	   is not very sensitive to the axial detection range. Even if it
	   is misestimated by 100-200 nm, it does not bias the parameter estimation of D_FREE or F_BOUND very much. 

# How to cite

If you use this script, please acknowledge the Spot-On manuscript to
which it is associated in your publications:

    Spot-On: robust model-based analysis of single-particle tracking experiments

    Anders S Hansen*, Maxime Woringer*, Jonathan B Grimm, Luke D Lavis, Robert Tjian, Xavier Darzacq
    bioRxiv preprint, Aug. 2017. doi:10.1101/171983.
	
    *These authors contributed equally and are alphabetically listed.

